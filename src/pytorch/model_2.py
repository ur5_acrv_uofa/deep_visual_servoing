#!/usr/bin/env python

import torch
import torch.nn as nn
import math
import torchvision
import torch.utils.model_zoo as model_zoo
import time
import numpy as np
import cv2



class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.relu = nn.ReLU(inplace=True)
        self.tanh = nn.Tanh()
        self.sigmoid = nn.Sigmoid()
        self.conv1 = nn.Conv2d(3,32,kernel_size=7, stride=2)
        self.conv2 = nn.Conv2d(32,64,kernel_size=5, stride=2)
        self.conv3 = nn.Conv2d(64,128,kernel_size=3, stride=2)
        self.conv4 = nn.Conv2d(128,256,kernel_size=3, stride=2)
        self.conv5 = nn.Conv2d(256,256,kernel_size=3, stride=2)
        self.conv6 = nn.Conv2d(256,128,kernel_size=1, stride=1)
        self.conv7 = nn.Conv2d(128,32,kernel_size=1, stride=1)
        self.bn0 = nn.BatchNorm2d(3)
        self.bn1 = nn.BatchNorm2d(32)
        self.bn2 = nn.BatchNorm2d(64)
        self.bn3 = nn.BatchNorm2d(128)
        self.bn4 = nn.BatchNorm2d(256)
        self.bn5 = nn.BatchNorm2d(256)
        self.bn6 = nn.BatchNorm2d(128)
        self.bn7 = nn.BatchNorm2d(32)
        s = 5
        c = 32
        self.avgpool = nn.AvgPool2d(s,stride=1)
        self.fc1 = nn.Linear(s*s*c+c,400)
        self.fc2 = nn.Linear(400,128)
        self.fc3 = nn.Linear(128, 64)
        self.fc4 = nn.Linear(64, 6)

        #Overwrite the default random weights
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def forward(self, x):
        x = self.bn0(x)
        x = self.relu(self.conv1(x))
        x = self.bn1(x)
        x = self.relu(self.conv2(x))
        x = self.bn2(x)
        x = self.relu(self.conv3(x))
        x = self.bn3(x)
        x = self.relu(self.conv4(x))
        x = self.bn4(x)
        x = self.relu(self.conv5(x))
        x = self.bn5(x)
        x = self.relu(self.conv6(x))
        x = self.bn6(x)
        x = self.relu(self.conv7(x))
        x = self.bn7(x)
        # print(x.shape)
        x_avg = self.avgpool(x)

        x = x.view(x.size(0), -1)
        x_avg = x_avg.view(x_avg.size(0), -1)
        # print(x.shape)
        # print(x_avg.shape)

        x = torch.cat((x,x_avg),1)
        # print(x.shape)
        x = self.sigmoid(self.fc1(x))
        x = self.sigmoid(self.fc2(x))
        x = self.sigmoid(self.fc3(x))
        x = self.tanh(self.fc4(x))
        
        # print(x.shape)
        return x

    def load_partial_state_dict(self,pretrained_dict):
        """This will only load weights for operations that exist in the model
        and in the state_dict"""

        state_dict = self.state_dict()

        # 1. filter out unnecessary keys
        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in state_dict}
        # 2. overwrite entries in the existing state dict
        state_dict.update(pretrained_dict)
        # 3. load the new state dict
        self.load_state_dict(state_dict)




if __name__ == "__main__":
    print("Hello There!")

    net = Net()

    net.eval()
    net.cuda()

    # normalize = torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])

    n = 1
    t1 = time.time()
    for i in range(n):
        img = torch.autograd.Variable(torch.rand(1,3,224,224)).float().cuda()
        out = net(img)
        out = out.data.cpu().numpy()
        label = np.argmax(out)
        print("Frame %i: %i" % (i,label))
    t2 = time.time()

    fps = n / (t2-t1)
    print("fps:",fps)

    weight_sum = 0
    for name,parameter in net.named_parameters():
        weights = np.prod(parameter.shape)
        weight_sum += weights
        print(name,weights)
    print("total weights",weight_sum)
    # try:
    #     cap = cv2.VideoCapture(0)
    #     n = 10
    #     fps = 0
    #     while(True):
    #         t1 = time.time()
    #
    #         for i in range(n):
    #             # Capture frame-by-frame
    #             ret, img = cap.read()
    #             frame = cv2.resize(img,(224,224))
    #             frame = np.transpose(frame,(2,0,1))
    #             frame = frame.astype(np.float) / 255.0
    #
    #
    #             frame = torch.from_numpy(frame).float().cuda()
    #             frame = normalize(frame)
    #             frame = torch.autograd.Variable(frame).float().cuda()
    #             frame = frame.unsqueeze(0)
    #             out = net(frame)
    #             out = out.data.cpu().numpy()
    #             label = np.argmax(out)
    #             print("FPS %2.1f: %s" % (fps,str(out)))
    #
    #             # Display the resulting frame
    #             cv2.imshow('frame',img)
    #
    #             if cv2.waitKey(10) & 0xFF == ord('q'):
    #                 break
    #         t2 = time.time()
    #         fps = n / (t2-t1)
    #
    #
    # except KeyboardInterrupt:
    #     pass
    # finally:
    #     # When everything done, release the capture
    #     cap.release()
    #     cv2.destroyAllWindows()
    #     print("All Closed")
