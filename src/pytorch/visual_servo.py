#!/usr/bin/env python2
import rospy
import torch
import cv2
import numpy as np

import sensor_msgs.msg
import deep_visual_servoing.msg
import cv_bridge
bridge = cv_bridge.CvBridge()
from torchvision import transforms

class VisualServo():

    def __init__(self):
        rospy.init_node('visual_servo', anonymous=False)

        self.ctrl_pub = rospy.Publisher("visual_control",deep_visual_servoing.msg.Control,queue_size=10)
        rospy.Subscriber("/camera/rgb/image_raw", sensor_msgs.msg.Image, self.image_callback)

        self.img_rgb = None

        self.net = torch.load("models/open_day_face_track_resnet_1.pt")
        # self.net = torch.load("face_track_nednet.pt")
        self.net.eval()
        self.net.cuda()

        cv2.namedWindow("Camera",0)
        self.loop()

    def loop(self):
        #create blank message
        msg = deep_visual_servoing.msg.Control()

        transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])
        ])

        out_smooth = np.zeros((6))
        smooth_a = 0.2

        #publish the latest control message at 30Hz
        r = rospy.Rate(30)

        while not rospy.is_shutdown():

            if not self.img_rgb is None:

                #crop the center of the image
                h,w,c = self.img_rgb.shape
                s = min(h,w)
                x1 = (w-s)/2
                x2 = x1 + s
                y1 = (h-s)/2
                y2 = y1 + s

                img_crop= self.img_rgb[y1:y2,x1:x2,:]

                img_resized = cv2.resize(img_crop,(224,224))
                cv2.imshow("Camera",img_resized[:,:,::-1])
                cv2.waitKey(1)
                # img_resized = img_resized[:,:,::-1].copy()
                img_tensor = transform(img_resized)
                # print(img_tensor)
                img_tensor.unsqueeze_(0)
                img_var = torch.autograd.Variable(img_tensor).cuda()

                out = self.net(img_var).cpu().data[0].numpy() * 1.8
                #low pass filter
                out_smooth += smooth_a * (out - out_smooth)


                msg = deep_visual_servoing.msg.Control()
                msg.vx,msg.vy,msg.vz,msg.rx,msg.ry,rz = out_smooth
                # print(msg)


            self.ctrl_pub.publish(msg)
            r.sleep()





    def image_callback(self,msg):
        img_rgb = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
        self.img_rgb = img_rgb

if __name__ == "__main__":

    VisualServo()
