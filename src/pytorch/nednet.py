#!/usr/bin/env python2
import torch
import torch.nn as nn
import math
import torchvision
import torch.utils.model_zoo as model_zoo
import time
import numpy as np
import cv2

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.sigmoid = nn.Sigmoid()
        self.relu = nn.ReLU()
        self.tanh = nn.Tanh()
        self.conv1 = nn.Conv2d(3, 32, kernel_size = 5, stride = 2)
        self.conv2 = nn.Conv2d(32, 32, kernel_size = 5, stride = 2)
        self.conv3 = nn.Conv2d(32, 64, kernel_size = 5, stride = 2)
        self.conv4 = nn.Conv2d(64, 64, kernel_size = 5, stride = 1)
        self.conv5 = nn.Conv2d(64, 128, kernel_size = 3, stride = 1)
        self.conv6 = nn.Conv2d(128, 256, kernel_size = 3, stride = 1)
        self.conv7 = nn.Conv2d(256, 1024, kernel_size = 3, stride = 1)
        self.conv8 = nn.Conv2d(1024, 256, kernel_size = 3, stride = 1)
        self.conv9 = nn.Conv2d(256, 64, kernel_size = 1, stride = 1)
        self.conv10 = nn.Conv2d(64, 32, kernel_size = 1, stride = 1)
        self.bn0 = nn.BatchNorm2d(3)
        self.bn1 = nn.BatchNorm2d(32)
        self.bn2 = nn.BatchNorm2d(32)
        self.bn3 = nn.BatchNorm2d(64)
        self.bn4 = nn.BatchNorm2d(64)
        self.bn5 = nn.BatchNorm2d(128)
        self.bn6 = nn.BatchNorm2d(256)
        self.bn7 = nn.BatchNorm2d(1024)
        self.bn8 = nn.BatchNorm2d(256)
        self.bn9 = nn.BatchNorm2d(64)
        self.bn10 = nn.BatchNorm2d(32)
        self.avgpool = nn.AvgPool2d(5,stride = 2)
        self.fc1 = nn.Linear(6208, 4104)
        self.fc2 = nn.Linear(4104, 2052)
        self.fc3 = nn.Linear(2052, 1026)
        self.fc4 = nn.Linear(1026, 6)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def forward(self, x):
        x = self.bn0(x)
        # print(x.shape)
        x = self.relu(self.conv1(x))
        # print(x.shape)
        x = self.bn1(x)
        x = self.relu(self.conv2(x))
        x = self.bn2(x)
        x = self.relu(self.conv3(x))
        x = self.bn3(x)
        x = self.relu(self.conv4(x))
        x = self.bn4(x)
        x = self.relu(self.conv5(x))
        x = self.bn5(x)
        x = self.relu(self.conv6(x))
        x = self.bn6(x)
        x = self.relu(self.conv7(x))
        x = self.bn7(x)
        x = self.relu(self.conv8(x))
        x = self.bn8(x)
        x = self.relu(self.conv9(x))
        x = self.bn9(x)
        x = self.relu(self.conv10(x))
        x = self.bn10(x)
        # print(x.shape)
        x_avg = self.avgpool(x)
        # print(x.shape)
        x = x.view(x.size(0), -1)
        x_avg = x_avg.view(x_avg.size(0), -1)
        x = torch.cat((x,x_avg),1)
        # print(x.shape)
        x = self.sigmoid(self.fc1(x))
        x = self.sigmoid(self.fc2(x))
        x = self.sigmoid(self.fc3(x))
        x = self.tanh(self.fc4(x))
        return x

if __name__ == "__main__":
    net = Net()
    img = torch.autograd.Variable(torch.randn(1,3,224,224))
    out = net(img)
    print(out)

    weight_sum = 0
    for name,parameter in net.named_parameters():
        weights = np.prod(parameter.shape)
        weight_sum += weights
        print(name,weights)
    print("total weights",weight_sum)
